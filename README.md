# docker-ros-noetic-vnc

## Quick Start

ワークスペースでdockerコンテナを実行し、`6080`ポートでアクセスします

```bash
docker run -p 6080:80 -v /dev/shm:/dev/shm -v $(pwd):/catkin_ws registry.gitlab.com/kuchitake/docker-ros-noetic-vnc
```

http://127.0.0.1:6080/

## CUI(bash)でログイン

```bash
docker exec -it **** /bin/bash
```
